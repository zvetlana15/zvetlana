package com.example.foodapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UserAdapter extends ArrayAdapter<User> {

    private Context context;

    public UserAdapter(Context iContext, ArrayList<User> users) {

        super(iContext,0,users);
        context = iContext;

    }



    @Override

    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position

        User user = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_list, parent, false);

        }

        // Lookup view for data population

        TextView vPublisher =(TextView)convertView.findViewById(R.id.text_publisher);
        TextView vTitle =(TextView)convertView.findViewById(R.id.text_title);

        ImageView vImage =(ImageView) convertView.findViewById(R.id.image_view);

        TextView vSocial =(TextView)convertView.findViewById(R.id.text_socialRank);


        // Populate the data into the template view using the data object


        vPublisher.setText("Creator: "+user.getmCreator());
        vTitle.setText("Title: "+user.getTitle());

        //image
        //  Picasso.with(context).load(user.getmImageUrl()).fit().centerInside().into(vImage);
        String aUrl = user.getmImageUrl().replace("http", "https");

        Picasso
                .with(context)
                .load(aUrl)
                .fit()
                .into(vImage);

        vSocial.setText(String.valueOf("Social Rank: "+user.getSocial_rank()));


        // Return the completed view to render on screen

        return convertView;

    }
}
