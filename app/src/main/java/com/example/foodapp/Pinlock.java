package com.example.foodapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class Pinlock extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinlock);
        Pinview pinview=(Pinview)findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                if(pinview.getValue().equals("1234"))
                {
                    closeKeyboard();
                    startActivity(new Intent(getApplicationContext(),Login.class));
                }
             else
                {
                    Toast.makeText(Pinlock.this,"Invalid Pin",Toast.LENGTH_SHORT).show();
                }
                //Toast.makeText(Pinlock.this,""+pinview.getValue(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void closeKeyboard()
    {
        View view =this.getCurrentFocus();
        if(view!=null)
        {
            InputMethodManager imm=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }
}
