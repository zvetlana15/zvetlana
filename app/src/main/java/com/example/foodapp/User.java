package com.example.foodapp;

public class User {

    private String mCreator;
    private String title;
    private int social_rank;
    private String ImageUrl;



    public User(){

        setCreator("");
        setTitle("");
        setSocialRank(0);
        setImage("");



    }

    public User(String icreator, String iTitle, int iSocialRank, String iimageUrl){

        setCreator(icreator);
        setTitle(iTitle);

        setImage(iimageUrl);

        setSocialRank(iSocialRank);

    }


    public String getmCreator() {
        return mCreator;
    }

    public void setCreator(String mCreator) {
        this.mCreator = mCreator;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getmImageUrl() {
        return ImageUrl;
    }

    public void setImage(String mImageUrl) {
        this.ImageUrl = mImageUrl;
    }



    public int getSocial_rank() {
        return social_rank;
    }

    public void setSocialRank(int social_rank) {
        this.social_rank = social_rank;
    }


}