package com.example.foodapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class timer extends AppCompatActivity {
    private EditText mEditTextInput;
    private TextView mTextViewCountDown;
    private Button mButtonSet;
    private Button mButtonStartPause;
    private Button mButtonReset;

    private CountDownTimer mCountDownTimer;

    private boolean mTimerRunning;

    private long mStartTimeInMillis;
    private long mTimeLeftInMillis;
    private long mEndTime;

    public static final String SHARED_PREFERENCES = "sharedPrefs";
    public static final String WORDS = "";
    String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        mEditTextInput=findViewById(R.id.edit_text_input);
        mTextViewCountDown=findViewById(R.id.text_view_countdown);
        mButtonSet=findViewById(R.id.button_set);
        mButtonStartPause=findViewById(R.id.button_start_pause);
        mButtonReset=findViewById(R.id.button_reset);

        mButtonSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input=mEditTextInput.getText().toString();
                if(input.length()==0)
                {
                    Toast.makeText(timer.this,"Field cant be empty",Toast.LENGTH_SHORT).show();
                    return;
                }
                long millisInput=Long.parseLong(input)*60000;
                if(millisInput==0)
                {
                    Toast.makeText(timer.this,"Please enter aa positive number",Toast.LENGTH_SHORT).show();
                    return;
                }
                setTime(millisInput);
               // mEditTextInput.setText("");

            }
        });

        mButtonStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTimerRunning){
                    pauseTimer();
                }
                else
                {
                    startTimer();
                }
            }
        });

        mButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();
            }
        });

        Button buttonPop = findViewById(R.id.btnPop);
        buttonPop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
            }
        });
        //updateCountDownText();

    }


    private void setTime(long milliseconds){
        mStartTimeInMillis=milliseconds;
        resetTimer();
        closeKeyboard();
    }
    private void startTimer()
    {
        mEndTime=System.currentTimeMillis()+mTimeLeftInMillis;
        mCountDownTimer= new CountDownTimer(mTimeLeftInMillis,1000) {
            @Override
            public void onTick(long millisUntilFinished)
            {
                mTimeLeftInMillis=millisUntilFinished;
                updateCountDownText();

            }

            @Override
            public void onFinish()
            {
                mTimerRunning=false;
                mButtonStartPause.setText("start");
                mButtonStartPause.setVisibility(View.INVISIBLE);
                mButtonReset.setVisibility(View.VISIBLE);

            }
        }.start();
        mTimerRunning=true;
        mButtonStartPause.setText("pause");
        mButtonReset.setVisibility(View.INVISIBLE);
    }

    private void pauseTimer()
    {
        mCountDownTimer.cancel();
        mTimerRunning=false;
        mButtonStartPause.setText("start");
        mButtonReset.setVisibility(View.VISIBLE);

    }


    private void resetTimer()
    {
        mTimeLeftInMillis=mStartTimeInMillis;
        updateCountDownText();
        mButtonReset.setVisibility(View.INVISIBLE);
        mButtonStartPause.setVisibility(View.VISIBLE);

    }
    private void updateCountDownText()
    {
        int hours=(int)(mTimeLeftInMillis/1000)/3600;
        int minutes=(int)((mTimeLeftInMillis/1000)%3600)/60;
        int seconds=(int)(mTimeLeftInMillis/1000)%60;

        String timeLeftFormatted;

        if(hours>0){
            timeLeftFormatted=String.format(Locale.getDefault(),"%d:%02d:%02d",minutes,seconds);
        }else{
            timeLeftFormatted=String.format(Locale.getDefault(),"%%02d:%02d",minutes,seconds);
        }

        timeLeftFormatted=String.format(Locale.getDefault(),"%02d:%02d",minutes,seconds);
        mTextViewCountDown.setText(timeLeftFormatted);
    }

    private void updateWatchInterface()
    {
        if(mTimerRunning){
            mEditTextInput.setVisibility(View.INVISIBLE);
            mButtonSet.setVisibility(View.INVISIBLE);
            mButtonReset.setVisibility(View.INVISIBLE);
            mButtonStartPause.setText("Pause");
        }else
        {
            mEditTextInput.setVisibility(View.VISIBLE);
            mButtonSet.setVisibility(View.VISIBLE);
            mButtonStartPause.setText("Start");

            if(mTimeLeftInMillis<1000){
                mButtonStartPause.setVisibility(View.INVISIBLE);
            }else{
                mButtonStartPause.setVisibility(View.VISIBLE);
            }
            if (mTimeLeftInMillis<mStartTimeInMillis){
                mButtonReset.setVisibility(View.VISIBLE);
            }else{
                mButtonReset.setVisibility(View.INVISIBLE);
            }
        }
    }
    private void closeKeyboard()
    {
        View view=this.getCurrentFocus();
        if(view!=null){
            InputMethodManager imm=(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        SharedPreferences prefs=getSharedPreferences("prefs",MODE_PRIVATE);
        SharedPreferences.Editor editor=prefs.edit();

        editor.putLong("startTimeInMillis",mStartTimeInMillis);
        editor.putLong("millisLeft",mTimeLeftInMillis);
        editor.putBoolean("timeRunning",mTimerRunning);
        editor.putLong("endTime",mEndTime);

        editor.apply();
        if(mCountDownTimer!=null){
            mCountDownTimer.cancel();
        }
        saveData();
    }

    @Override
    protected  void onStart()
    {
        super.onStart();
        SharedPreferences prefs=getSharedPreferences("prefs",MODE_PRIVATE);

        mStartTimeInMillis=prefs.getLong("startTimeInMillis",600000);
        mTimeLeftInMillis=prefs.getLong("millisLeft",mStartTimeInMillis);
        mTimerRunning=prefs.getBoolean("timerRunning",false);

        updateCountDownText();
        updateWatchInterface();

        if(mTimerRunning)
        {
            mEndTime=prefs.getLong("endTime",0);
            mTimeLeftInMillis=mEndTime-System.currentTimeMillis();
            if(mTimeLeftInMillis<0)
            {
                mTimeLeftInMillis=0;
                mTimerRunning=false;
                updateCountDownText();
                updateWatchInterface();
            }else {
                startTimer();
            }
        }
        loadData();
        update();
    }



    @Override
    public void onResume() {
        super.onResume();
        loadData();
        update();

    }
    @Override
    public void onPause() {
        super.onPause();
        saveData();

    }
    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES,MODE_PRIVATE);
        text = sharedPreferences.getString(WORDS, "");

    }

    public void update()
    {
        mEditTextInput.setText(text);
    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(WORDS, mEditTextInput.getText().toString());

        editor.apply();
    }

    public void showPopup(View v){
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()){

                    case R.id.pitm1:
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        break;
                    default: return false;

                }
                return true;
            }
        });

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pop, popupMenu.getMenu());
        popupMenu.show();
    }




}
