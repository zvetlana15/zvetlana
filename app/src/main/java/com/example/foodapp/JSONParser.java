package com.example.foodapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONParser {
    public static ArrayList<User> getUserList(String data) throws JSONException {

        ArrayList<User> userList = new ArrayList<>();

        JSONObject obj = new JSONObject(data);
        JSONArray users = obj.getJSONArray("recipes");

        for (int i = 0; i < users.length(); i++){

            JSONObject user = users.getJSONObject(i);
            userList.add(new User (user.getString("publisher"),user.getString("title"),user.getInt("social_rank"),user.getString("image_url")));

        }
        return userList;

    }
}
