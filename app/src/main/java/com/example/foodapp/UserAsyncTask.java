package com.example.foodapp;

import android.os.AsyncTask;
import android.widget.ArrayAdapter;

import org.json.JSONException;

public class UserAsyncTask extends AsyncTask<Void, Void, String> {
    ArrayAdapter lvUsers;


    public UserAsyncTask(ArrayAdapter aa){
        lvUsers = aa;
    }
    @Override
    protected String doInBackground(Void... voids) {

        HTTPClient client = new HTTPClient();
        String data = client.getJSON("https://www.food2fork.com/api/search?key=ae3bb3c0f45bbb2b961c926a92132a50");

        return data;
    }

    public void onPostExecute(String data){

        try {

            lvUsers.addAll(JSONParser.getUserList(data));
            lvUsers.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
