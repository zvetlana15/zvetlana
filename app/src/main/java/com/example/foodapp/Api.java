package com.example.foodapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class Api extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api);

        ListView lv = findViewById(R.id.listView);

        UserAdapter userAdapter = new UserAdapter(this, new ArrayList<User>());

        lv.setAdapter(userAdapter);

        UserAsyncTask uTask = new UserAsyncTask(userAdapter);
        uTask.execute();
    }
}
