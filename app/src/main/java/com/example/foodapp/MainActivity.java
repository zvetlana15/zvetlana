package com.example.foodapp;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.foodapp.adapters.NotesAdapter;
import com.example.foodapp.db.NotesDB;
import com.example.foodapp.db.NotesDao;
import com.example.foodapp.model.Note;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
private RecyclerView recyclerView;
private ArrayList<Note> notes;
private NotesAdapter adapter;
private NotesDao dao;
public static final String DATE_SHARED_PREFERENCES = "dateSharedPrefs";
public static final String DATETIME = "";
String dateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView=findViewById(R.id.notes_list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            onAddNewNote();
            }
        });
        dao= NotesDB.getInstance(this).notesDao();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void loadNotes() {
        this.notes=new ArrayList<>();
        List<Note> list= dao.getNotes();
        this.notes.addAll(list);
        this.adapter= new NotesAdapter(this,notes);
        this.recyclerView.setAdapter(adapter);
    }



    private void onAddNewNote() {
        startActivity(new Intent(this,EditeNoteActivity.class));

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.login) {
            // Handle login
            startActivity(new Intent(getApplicationContext(),Login.class));
        } else if (id == R.id.memo) {
// Handle memo
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        } else if (id == R.id.camera) {
            startActivity(new Intent(getApplicationContext(),TakePhoto.class));
// Handle camera
        }
        else if (id == R.id.album) {
            startActivity(new Intent(getApplicationContext(),Album.class));
// Handle album
        }else if (id == R.id.recipe) {
// Handle api
            startActivity(new Intent(getApplicationContext(),Api.class));

        } else if (id == R.id.timer) {
// Handle timer
            startActivity(new Intent(getApplicationContext(),timer.class));
        }
        else if (id == R.id.logout) {
// Handle timer
            startActivity(new Intent(getApplicationContext(),logOut.class));
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadNotes();
        loadData();
        update();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }


    @Override
    protected void onStart() {
        super.onStart();
        loadData();
        update();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveData();
    }

    private void saveData() {

        Date currentTime = Calendar.getInstance().getTime();

        SharedPreferences sharedPreferences = getSharedPreferences(DATE_SHARED_PREFERENCES,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(DATETIME, currentTime.toString());

        editor.apply();
    }

    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(DATE_SHARED_PREFERENCES, MODE_PRIVATE);
        dateTime = sharedPreferences.getString(DATETIME, "");

    }

    public void update()
    {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.content_main),"Last Activitiy: " + dateTime,Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
